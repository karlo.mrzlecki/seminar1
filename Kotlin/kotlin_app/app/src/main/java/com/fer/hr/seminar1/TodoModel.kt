package com.fer.hr.seminar1

import java.sql.Date


data class TodoModel(
    var id: Int,
    var todo: String,
    var creationDate: String,
    var isChecked: Boolean
)