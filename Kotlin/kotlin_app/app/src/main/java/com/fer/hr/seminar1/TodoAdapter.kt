package com.fer.hr.seminar1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class TodoAdapter: RecyclerView.Adapter<TodoAdapter.TodoViewHolder>() {

    private var todoList: ArrayList<TodoModel> = ArrayList()

    private var onClickDeleteItem: ((TodoModel) -> Unit)? = null
    private var onClickUpdateItem: ((TodoModel) -> Unit)? = null

    fun addItems(items: ArrayList<TodoModel>){
        this.todoList = items
        notifyDataSetChanged()
    }

    class TodoViewHolder(view: View): RecyclerView.ViewHolder(view){
        private var id = view.findViewById<TextView>(R.id.tv_todo_id)
        private var title = view.findViewById<TextView>(R.id.tv_todo_title)
        private var date = view.findViewById<TextView>(R.id.tv_todo_date)
        var btnDelete = view.findViewById<Button>(R.id.btn_delete)
        var checkbox = view.findViewById<CheckBox>(R.id.cb_todo)

        fun bindView(todo: TodoModel){
//            id.text = todo.id.toString()
            title.text = todo.todo
            date.text = todo.creationDate
            checkbox.isChecked = todo.isChecked
        }
    }

    fun setOnClickDeleteItem(callback:(TodoModel) -> Unit){
        this.onClickDeleteItem = callback
    }

    fun setOnClickUpdateItem(callback:(TodoModel) -> Unit){
        this.onClickUpdateItem = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TodoViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.card_item_todo, parent, false)
    )

    override fun getItemCount(): Int {
        return todoList.size
    }

    override fun onBindViewHolder(holder: TodoAdapter.TodoViewHolder, position: Int) {
        val todo = todoList[position]
        holder.bindView(todo)
        holder.btnDelete.setOnClickListener { onClickDeleteItem?.invoke(todo) }
        holder.checkbox.setOnClickListener { onClickUpdateItem?.invoke(todo) }
    }


}