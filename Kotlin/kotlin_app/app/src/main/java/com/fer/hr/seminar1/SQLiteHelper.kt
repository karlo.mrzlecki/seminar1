package com.fer.hr.seminar1

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.TableLayout
import androidx.core.content.contentValuesOf
import java.lang.Exception

class SQLiteHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object{

        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "todo_db"
        private const val TABLE_NAME = "todo"
        private const val ID = "id"
        private const val TODO = "todo"
        private const val DATE = "creationDate"
        private const val CHECKED = "isChecked"

    }

    override fun onCreate(db: SQLiteDatabase?) {
       val createTable = ("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TODO + " TEXT,"
            + DATE + " TEXT,"
            + CHECKED + " INTEGER)")
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun updateTodo(todoEntry: TodoModel): Int{
        val db = this.writableDatabase

        val contentValues = contentValuesOf()
        contentValues.put(TODO, todoEntry.todo)
        contentValues.put(DATE, todoEntry.creationDate)
        contentValues.put(CHECKED, !todoEntry.isChecked)

        val success = db.update(TABLE_NAME, contentValues, "id=" + todoEntry.id, null)
        db.close()
        return success
    }

    fun deleteTodo(id: Int): Int{
        val db = this.writableDatabase

        val contentValues = contentValuesOf()
        contentValues.put(ID, id)

        val success = db.delete(TABLE_NAME,"id=$id", null)
        db.close()
        return success
    }

    fun insertTodo(todoEntry: TodoModel): Long{
        val db = this.writableDatabase

        val contentValues = contentValuesOf()
        contentValues.put(TODO, todoEntry.todo)
        contentValues.put(DATE, todoEntry.creationDate)
        contentValues.put(CHECKED, todoEntry.isChecked)

        val success = db.insert(TABLE_NAME, null, contentValues)
        db.close()
        return success
    }

    fun getTodo(): ArrayList<TodoModel>{
        val todoList: ArrayList<TodoModel> = ArrayList()
        val select = "SELECT * FROM $TABLE_NAME ORDER BY $ID DESC"
        val db = this.readableDatabase

        val cursor: Cursor?

        try {
            cursor = db.rawQuery(select, null)
        }catch (e: Exception){
            e.printStackTrace()
            db.execSQL(select)
            return ArrayList()
        }

        var id: Int
        var todo: String
        var creationDate: String
        var isChecked: Boolean

        if (cursor.moveToFirst()){
            do{
                id = cursor.getInt(cursor.getColumnIndexOrThrow("id"))
                todo = cursor.getString(cursor.getColumnIndexOrThrow("todo"))
                creationDate = cursor.getString(cursor.getColumnIndexOrThrow("creationDate"))
                isChecked = cursor.getInt(cursor.getColumnIndexOrThrow("isChecked")) > 0

                val todoEntry = TodoModel(id, todo, creationDate, isChecked)
                todoList.add(todoEntry)
            } while (cursor.moveToNext())
        }

        return todoList
    }
}