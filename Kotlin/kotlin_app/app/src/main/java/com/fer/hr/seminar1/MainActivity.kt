package com.fer.hr.seminar1

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var etTodo: EditText
    private lateinit var btnAdd: Button

    private lateinit var sqLiteHelper: SQLiteHelper
    private lateinit var recyclerview: RecyclerView
    private var adapter: TodoAdapter ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        initRecyclerView()

        sqLiteHelper = SQLiteHelper(this)

        btnAdd.setOnClickListener { addTodo() }

        getTodo()

        adapter?.setOnClickDeleteItem {
            deleteTodo(it.id)
        }

        adapter?.setOnClickUpdateItem {
            updateTodo(it)
        }
    }

    private fun initView(){
        etTodo = findViewById(R.id.et_todo)
        btnAdd = findViewById(R.id.btn_add)
        recyclerview = findViewById(R.id.recyclerView)
    }

    private fun initRecyclerView(){
        recyclerview.layoutManager = LinearLayoutManager(this)
        adapter = TodoAdapter()
        recyclerview.adapter = adapter
    }

    private fun addTodo() {
        val todo = etTodo.text.toString()
        val sdf = SimpleDateFormat("YYYY-MM-DD HH:MM:SS", Locale.ENGLISH)
        val date: String = sdf.format(Date())

        if(todo.isEmpty()){
            Toast.makeText(this, "Please enter a todo", Toast.LENGTH_SHORT).show()
        } else {
            val todo = TodoModel(0, todo, date, false)
            val status = sqLiteHelper.insertTodo(todo)
            if (status > -1){
                etTodo.setText("")
                etTodo.requestFocus()
                getTodo()
            }
        }

    }

    private fun deleteTodo(id: Int){
        if(id == null) return

        sqLiteHelper.deleteTodo(id)
        getTodo()
    }

    private fun updateTodo(todo: TodoModel){
        if(todo == null) return

        sqLiteHelper.updateTodo(todo)
        getTodo()
    }

    private fun getTodo(){
        val todoList = sqLiteHelper.getTodo()
        adapter?.addItems(todoList)
    }
}