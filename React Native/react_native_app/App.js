import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  FlatList,
} from "react-native";
import { openDatabase } from "react-native-sqlite-storage";
import { Checkbox } from "react-native-paper";

// open a connection to database
const db = openDatabase({
  // this is the name of our database.
  name: "todo_db",
});

const App = () => {
  // the React Hooks
  // this is used to control states of different elements in the app
  const [todo, setTodo] = useState("");
  const [todos, setTodos] = useState([]);

  const [checked, setChecked] = useState([]);

  // the createTables function
  // this creates Tables in our database
  const createTables = () => {
    db.transaction((txn) => {
      txn.executeSql(
        `CREATE TABLE IF NOT EXISTS todo (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            title TEXT,
            creationDate TEXT,
            isChecked TEXT)`,
        [],
        (sqlTxn, res) => {
          console.log("table created successfully");
        },
        (error) => {
          console.log("error on creating table " + error.message);
        }
      );
    });
  };

  // function to add data into our database
  const insertTodo = () => {
    // check if our todo message is empty
    if (!todo) {
      alert("add new todo");
      return false;
    }

    // get the current date and time for creationDate field
    const date = new Date().toLocaleString();

    // insert the todo
    db.transaction((txn) => {
      txn.executeSql(
        `INSERT INTO todo (title, creationDate, isChecked) VALUES (?, ?, ?)`,
        [todo, date, "NOT DONE"],
        (sqlTxn, res) => {
          console.log(`${todo} todo added successfully`);
          getTodos(); // call Hooks to update the todo fields
          setTodo(""); // set Hook to empty the input field
        },
        (error) => {
          console.log("error on adding todo " + error.message);
        }
      );
    });
  };

  // function to fetch all the todo data from our database
  const getTodos = () => {
    db.transaction((txn) => {
      txn.executeSql(
        `SELECT * FROM todo ORDER BY id DESC`,
        [],
        (sqlTxn, res) => {
          console.log("todos retrieved successfully");
          let len = res.rows.length;

          if (len > 0) {
            let results = [];
            for (let i = 0; i < len; i++) {
              let item = res.rows.item(i); // get one item from our todo list
              results.push({
                id: item.id,
                title: item.title,
                date: item.creationDate,
                isChecked: item.isChecked,
              }); // create map from query fields and add it to results list
            }

            setTodos(results); // call Hook to show todos on the screen
          }
        },
        (error) => {
          console.log("error on getting todos " + error.message);
        }
      );
    });
  };

  // function to delete a todo from our database
  const deleteTodo = (id) => {
    db.transaction((txn) => {
      txn.executeSql(
        `DELETE FROM todo WHERE id = (?)`,
        [id],
        (sqlTxn, res) => {
          console.log(`${todo} todo deleted successfully`);
          getTodos();
          setTodos("");
        },
        (error) => {
          console.log("error on deleting todo " + error.message);
        }
      );
    });
  };

  // function to change a state of todo
  const toggleCheck = (id, isChecked) => {
    isChecked = isChecked === "DONE" ? "NOT DONE" : "DONE";
    db.transaction((txn) => {
      txn.executeSql(
        `UPDATE todo SET isChecked = (?) WHERE id = (?)`,
        [isChecked, id],
        (sqlTxn, res) => {
          console.log(`${todo} todo deleted successfully`);
          getTodos();
          setTodos("");
        },
        (error) => {
          console.log("error on deleting todo " + error.message);
        }
      );
    });
  };

  // function to render a todo on screen
  const renderTodo = ({ item }) => {
    // React-Native does no longer suport checkbox so we use colors to represent state of a todo
    var color = item.isChecked === "NOT DONE" ? "#FF5733" : "#90EE90";

    return (
      <View
        style={{
          paddingVertical: 12,
          paddingHorizontal: 10,
          borderBottomWidth: 1,
          borderColor: "#ddd",
          flexDirection: "row",
        }}
      >
        <Button
          color={color}
          onPress={() => toggleCheck(item.id, item.isChecked)}
          id={item.id}
          title={item.isChecked}
        />

        <View style={{ paddingHorizontal: 10, flexDirection: "column" }}>
          <Text>{item.title}</Text>
          <Text>{item.date}</Text>
        </View>
        <View
          style={{
            position: "absolute",
            right: 10,
            paddingVertical: 15,
          }}
        >
          <Button
            onPress={() => deleteTodo(item.id, item.isChecked)}
            id={item.id}
            title="X"
          />
        </View>
      </View>
    );
  };

  // React useEffect hooks which do work after rendering the components
  useEffect(() => {
    createTables(); // call our function to create database if it doesn't exist
    getTodos(); // get all the todos from database
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <StatusBar backgroundColor="#222" />

      <FlatList data={todos} renderItem={renderTodo} key={(item) => item.id} />

      <View style={{ width: "100%", flexDirection: "row" }}>
        <TextInput
          placeholder="add new todo"
          value={todo}
          onChangeText={setTodo}
          style={{ width: "87%", marginHorizontal: 8 }}
        />

        <Button title="Add" onPress={insertTodo} />
      </View>
    </View>
  );
};

export default App;